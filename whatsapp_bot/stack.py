from yowsup.stacks import YowStackBuilder

from state_machine.state_machine import StateMachine
from state_machine.models.message.message_factory import MessageFactory
from .layer import EchoLayer
from yowsup.layers import YowLayerEvent
from yowsup.layers.network import YowNetworkLayer


class YowsupEchoStack(object):
    def __init__(self, credentials, message_factory: MessageFactory, state_machine: StateMachine):
        stack_builder = YowStackBuilder()

        self._stack = stack_builder \
            .pushDefaultLayers() \
            .push(EchoLayer(message_factory, state_machine)) \
            .build()

        self._stack.setCredentials(credentials)

    def set_prop(self, key, val):
        self._stack.setProp(key, val)

    def start(self):
        self._stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
        self._stack.loop()
