import logging
import random
from time import sleep

from sqlalchemy.orm import Session
from yowsup.layers.interface import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_messages.protocolentities import TextMessageProtocolEntity

from core.model.base import session_factory
from core.model.message import Message
from core.model.utils import get_or_create
from core.model.user import User
from state_machine.models.message.message_factory import MessageFactory
from state_machine.state_machine import StateMachine

logger = logging.getLogger(__name__)


class EchoLayer(YowInterfaceLayer):
    def __init__(self, message_factory: MessageFactory, state_machine: StateMachine):
        super().__init__()

        self._message_factory = message_factory
        self._state_machine = state_machine

    @ProtocolEntityCallback("message")
    def onMessage(self, message):
        internal_message = self._message_factory.create_from_whatsapp_message(message)

        # If we get an empty message, we just ignore it and acknowledge it.
        if internal_message is None:
            logger.error('Got an unsupported message: {}'.format(message))
            self.toLower(message.ack())
            self.toLower(message.ack(True))
            return

        session = session_factory()
        user = self.__saveMessage(session, message)

        response_messages = self._state_machine.process_message(session, user, internal_message)
        session.close()

        logger.info(
            "Going to send action message '{0}' to {1} as reply to '{2}'."
            .format(response_messages, message.getFrom(False), self.__getContent(message))
        )

        for response in response_messages:
            sleep(random.randint(2, 15))
            self.toLower(TextMessageProtocolEntity(response, to=message.getFrom()))

        self.toLower(message.ack())
        self.toLower(message.ack(True))

    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        self.toLower(entity.ack())

    def __saveMessage(self, session: Session, message):
        user = get_or_create(session, User, phone_number=message.getFrom(False))
        content = self.__getContent(message)

        database_message = Message(
            message.getId(),
            user,
            content,
            message.getTimestamp(),
            message.getType(),
            user.state
        )

        session.add(database_message)
        session.commit()

        return user

    def __getContent(self, message):
        if message.getType() == 'text':
            return message.getBody()
        elif message.getType() == 'media' and message.media_type == "image":
            return message.url
        else:
            return None