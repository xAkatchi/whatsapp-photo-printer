import os
import pathlib

from PIL import Image
from datetime import datetime
from fpdf import FPDF


class ImageToPDFConverter:
    def __init__(self, pdf_width, pdf_height):
        self._pdf = FPDF(orientation='L', unit='mm', format=(pdf_height, pdf_width))
        self._pdf.set_auto_page_break(0)
        self._pdf_width = pdf_width
        self._pdf_height = pdf_height

    def create_pdf(self, photos, pdf_upload_path):
        for photo in photos:
            self._pdf.add_page()
            path = photo.file_path

            image = Image.open(path)

            photo_height = self._pdf_height
            photo_width = self._pdf_width

            if image.height > image.width:
                path = self._create_file_path(path)
                image = self._rotate_image(image, path)

            if (image.width / self._pdf_width) < (image.height / self._pdf_height):
                photo_height = 0
            else:
                photo_width = 0

            self._pdf.image(path, x=0, y=0, w=photo_width, h=photo_height)

        self._pdf.output(pdf_upload_path, "F")

    def _rotate_image(self, image, path_to_storage_image):
        image = image.rotate(90, expand=True)
        image.save(path_to_storage_image)

        return image

    def _create_file_path(self, current_image_path):
        file_name, ext = os.path.splitext(os.path.basename(current_image_path))
        now = datetime.now()

        folder_path = "/mnt/nfs/storage/uploads-rotated/{0}/{1}/{2}/" \
            .format(now.year, now.month, now.day)

        new_path = "{0}{1}{2}" \
            .format(folder_path, file_name, ext)

        pathlib.Path(folder_path).mkdir(parents=True, exist_ok=True)

        return new_path
