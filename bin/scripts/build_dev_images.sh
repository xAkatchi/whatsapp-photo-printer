#!/usr/bin/env bash

# Make sure that we base this script from the current directory instead of the invocation
# place [https://stackoverflow.com/a/12761885]
BASEDIR=$(dirname $0)
cd ${BASEDIR} || exit

PYTHON_VERSION="3.7"
PYTHON_BASE_IMAGE="python_base:${PYTHON_VERSION}"
API_IMAGE="api:latest"
BOT_WHATSAPP_IMAGE="bot_whatsapp:latest"

sh build_base_python.sh -v ${PYTHON_VERSION} -n ${PYTHON_BASE_IMAGE} -p false
sh build_api.sh -b ${PYTHON_BASE_IMAGE} -n ${API_IMAGE} -p false
sh build_bot_whatsapp_bot.sh -b ${PYTHON_BASE_IMAGE} -n ${BOT_WHATSAPP_IMAGE} -p false
