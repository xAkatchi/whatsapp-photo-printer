#!/usr/bin/env bash

usage() {
    echo "Usage: $0
        [-p <string> | phone number]
        [-c <string> | cc number]
        [-n <string> | whatsapp name]
        [-m <string> | mcc]
        [-o <string> | mnc]
        [-i <string> | whatsapp cli docker image]
        [-t <path> | Main directory that contains the yowsup folder (absolute path, no trailing slash)]"
        1>&2; exit 1;
}

# Make sure that the input parameters are properly set, otherwise we print usage
# https://stackoverflow.com/a/16496491
while getopts ":p:c:n:m:o:i:t:" o; do
    case "${o}" in
        p)
            PHONE_NUMBER=${OPTARG}
            ;;
        c)
            CC_NUMBER=${OPTARG}
            ;;
        n)
            PUSHNAME=${OPTARG}
            ;;
        m)
            MCC=${OPTARG}
            ;;
        o)
            MNC=${OPTARG}
            ;;
        i)
            CLI_IMAGE=${OPTARG}
            ;;
        t)
            PATH=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${PHONE_NUMBER}" ] ||
   [ -z "${CC_NUMBER}" ] ||
   [ -z "${PUSHNAME}" ] ||
   [ -z "${MCC}" ] ||
   [ -z "${MNC}" ] ||
   [ -z "${CLI_IMAGE}" ] ||
   [ -z "${PATH}" ]; then
    usage
fi

# Everything is set, now we can do the heavy lifting

echo "Going to request a registration code."

# TODO this must be executed from the home folder (also the docker run further down)
# Ideally shouldn't be needed, but ok for v1
docker run -v ${PATH}/yowsup:/root/.config/yowsup ${CLI_IMAGE} registration \
    --requestcode sms \
    --config-phone=${PHONE_NUMBER} \
    --config-cc=${CC_NUMBER} \
    --config-pushname=${PUSHNAME} \
    --config-mcc=${MCC} \
    --config-mnc=${MNC}

echo "Please enter the registration code."

read -n 6 -p "Whatsapp registration code:" REGISTRATION_CODE

echo "Registering..."

docker run -v ${PATH}/yowsup:/root/.config/yowsup ${CLI_IMAGE} registration \
    --register=${REGISTRATION_CODE} \
    --config-phone=${PHONE_NUMBER}

echo "Done"