#!/usr/bin/env bash

usage() { echo "Usage: $0 [-b <string> | python base image] [-n <string> | image name] [-p <boolean> | push the image]" 1>&2; exit 1; }

# Make sure that the input parameters are properly set, otherwise we print usage
# https://stackoverflow.com/a/16496491
while getopts ":b:n:p:" o; do
    case "${o}" in
        b)
            python_base_image=${OPTARG}
            ;;
        n)
            image_name=${OPTARG}
            ;;
        p)
            push=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${python_base_image}" ] || [ -z "${image_name}" ] || [ -z "${push}" ]; then
    usage
fi

# Everything is set, now we can continue

# Make sure that we base this script from the current directory instead of the invocation
# place [https://stackoverflow.com/a/12761885]
BASEDIR=$(dirname $0)
cd ${BASEDIR} || exit

cd ../docker/api || exit

echo "Building the docker image ${image_name}"
docker build --build-arg PYTHON_BASE_IMAGE=${python_base_image} -t ${image_name} .

if [ "${push}" = true ]; then
  echo "Pushing the image: ${image_name}"
  docker push ${image_name}
fi