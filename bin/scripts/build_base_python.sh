#!/usr/bin/env bash

usage() { echo "Usage: $0 [-v <string> | python version] [-n <string> | image name] [-p <boolean> | push the image]" 1>&2; exit 1; }

# Make sure that the input parameters are properly set, otherwise we print usage
# https://stackoverflow.com/a/16496491
while getopts ":v:n:p:" o; do
    case "${o}" in
        v)
            python_version=${OPTARG}
            ;;
        n)
            image_name=${OPTARG}
            ;;
        p)
            push=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${python_version}" ] || [ -z "${image_name}" ] || [ -z "${push}" ]; then
    usage
fi

# Everything is set, now we can continue

# Make sure that we base this script from the current directory instead of the invocation
# place [https://stackoverflow.com/a/12761885]
BASEDIR=$(dirname $0)
cd ${BASEDIR} || exit

cd ../docker/base_python || exit

# Make sure that the requirements file is available when building the base
echo "Copying requirements.txt"
cp ../../../requirements.txt .

echo "Building the docker image ${image_name}"
docker build --build-arg PYTHON_VERSION=${python_version} -t ${image_name} .

if [ "${push}" = true ]; then
  echo "Pushing the image: ${image_name}"
  docker push ${image_name}
fi

# Remove the requirements again after we're done with them
echo "Removing the requirements.txt"
rm requirements.txt