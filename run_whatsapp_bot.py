import logging
import os
import sys
import sentry_sdk
from datetime import datetime

from pyPostcode import Api
from yowsup.config.manager import ConfigManager

from core import seeder
from core.model.base import session_factory
from core.printapiclient import PrintAPIClient
from whatsapp_bot import YowsupEchoStack
from whatsapp_bot.media_downloader import MediaDownloader
from state_machine.models.action.action_factory import ActionFactory
from state_machine.models.message.message_factory import MessageFactory
from state_machine.state_machine import StateMachine

logging.basicConfig(
    level=logging.INFO,  # TODO move to env variable
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger(__name__)


def __get_message_factory() -> MessageFactory:
    now = datetime.now()

    upload_path = \
        "/mnt/nfs/storage/uploads/{0}/{1}/{2}"\
        .format(now.year, now.month, now.day)

    return MessageFactory(MediaDownloader(upload_path))


def __get_action_factory() -> ActionFactory:
    postcode_api_key = os.environ.get('POSTCODE_API_KEY')

    if postcode_api_key is None:
        logger.error("POSTCODE_API_KEY is missing.")
        sys.exit(1)

    return ActionFactory(Api(postcode_api_key), PrintAPIClient())


def __get_state_machine(action_factory: ActionFactory) -> StateMachine:
    return StateMachine(action_factory)


def run():
    logger.info('Starting')
    phone_number = os.environ.get('PHONE_NUMBER', None)

    if phone_number is None:
        logger.error("Please specify the PHONE_NUMBER variable.")
        sys.exit(1)

    session = session_factory()
    seeder.seed_database(session)
    session.close()

    # Load the whatsapp client
    config_manager = ConfigManager()
    config = config_manager.load(phone_number)

    credentials = config.phone, config.client_static_keypair

    stack = YowsupEchoStack(
        credentials,
        __get_message_factory(),
        # TODO this might cause database has gone away since we keep the session open for so long?
        __get_state_machine(__get_action_factory())
    )
    stack.start()


if __name__ == "__main__":
    sentry_sdk.init(os.environ.get('SENTRY_DSN'))

    try:
        run()
    except Exception as e:
        logger.error("Got an exception: {}", e)
        sys.exit(1)
