from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Optional, List

from sqlalchemy.orm import Session

from core.model.user import User
from state_machine.models.message.message import Message


class Action(ABC):
    def __init__(self):
        self._messages = []

    @abstractmethod
    def can_handle(self, user: User, message: Message) -> bool:
        pass

    @abstractmethod
    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        """
        :param session:
        :param user:
        :param message:
        :return next_action: The next Action (if any)
        """
        pass

    def get_messages(self) -> List[str]:
        return self._messages
