from typing import Optional

from sqlalchemy.orm import Session

from core.model.order import Order
from core.model.photo import Photo
from core.model.product import Product, PHOTO_15x10
from core.model.state import START, ORDER_CHECKOUT_ADDRESS
from core.model.user import User
from state_machine.models.action.action import Action
from state_machine.models.message.message import TextMessage, ImageMessage, Message


class StartAction(Action):
    def __init__(self, next_action: Action):
        super().__init__()

        self._next_action = next_action

    def can_handle(self, user: User, message: Message) -> bool:
        return user.state.state == START

    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        order = user.get_order_in_progress(session)

        if order is None:
            order = Order(user)
            order.product = session.query(Product).filter_by(description=PHOTO_15x10).first()
            session.add(order)
            session.commit()

        amount_of_photos = len(order.photos)

        if isinstance(message, TextMessage):
            if message.get_body().lower() == 'klaar' and amount_of_photos > 0:
                user.set_state(session, ORDER_CHECKOUT_ADDRESS)
                session.commit()
                return self._next_action

            self._messages.append(self._get_action_message(order))

            return None

        if amount_of_photos >= 100:
            self._messages.append(
                'Je zit over de limiet van 100 foto\'s heen, de extra foto\'s worden niet opgeslagen!'
            )
            return None

        if isinstance(message, ImageMessage):
            # No message if the user has uploaded a photo to prevent a ton of spam.
            session.add(Photo(file_path=message.get_image_path(), order=order))
            session.commit()

        return None

    def _get_action_message(self, order: Order) -> str:
        return """Hey, leuk je te zien!

Stuur de fotos die je graag wilt laten printen!
Elke foto kost €{0:.2f} + verzendkosten

Als je klaar bent, type dan \'Klaar\', dan gaan we je bestelling verwerken!

💡 Tip: Kies minimaal 8 (max. 100) fotos om dubbele fotos te voorkomen!

(Stuur op elk gewenst moment 'stop' om terug te gaan naar het begin.)"""\
            .format(
                order.product.price_per_piece / 100
            )