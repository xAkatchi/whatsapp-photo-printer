from typing import Optional

from sqlalchemy.orm import Session

from core.model.state import START
from core.model.user import User
from state_machine.models.action.action import Action
from state_machine.models.message.message import TextMessage, Message


class StopAction(Action):
    def __init__(self, next_action: Action):
        super().__init__()

        self._next_action = next_action

    def can_handle(self, user: User, message: Message) -> bool:
        if isinstance(message, TextMessage):
            return message.get_body().lower() == 'stop'

        return False

    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        user.set_state(session, START)
        order = user.get_order_in_progress(session)

        if order:
            order.in_progress = False

        session.commit()
        return self._next_action
