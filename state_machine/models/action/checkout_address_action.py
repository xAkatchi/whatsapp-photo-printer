import logging
import re
from typing import Optional

from pyPostcode import Api
from sqlalchemy.orm import Session

from core.model.address import Address
from core.model.order import Order
from core.model.state import ORDER_CHECKOUT_ADDRESS, ORDER_CHECKOUT_NAME
from core.model.user import User
from core.model.utils import get_or_create
from state_machine.models.action.action import Action
from state_machine.models.action.stop_action import StopAction
from state_machine.models.message.message import Message, TextMessage

logger = logging.getLogger(__name__)


class CheckoutAddressAction(Action):
    def __init__(self, postcode_api: Api, next_action: Action):
        super().__init__()

        self._postcode_api = postcode_api
        self._next_action = next_action

    def can_handle(self, user: User, message: Message) -> bool:
        return user.state.state == ORDER_CHECKOUT_ADDRESS

    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        order = user.get_order_in_progress(session)

        if isinstance(message, TextMessage) and self._is_valid_zip_code(message.get_body()):
            address = self._fetch_address_from_input(session, message.get_body())

            if address is None:
                logger.error('Failed to find an address for the given zip code "{0}".', message.get_body())
                self._messages.append('Sorry, ik kan je adres niet vinden, weet je zeker dat het klopt?')
                return None

            order.address = address
            user.set_state(session, ORDER_CHECKOUT_NAME)
            session.commit()
            return self._next_action

        self._messages.append(self._get_action_message(order))

        return None

    def _is_valid_zip_code(self, input: str) -> bool:
        return bool(re.search(r"\d{4}[a-zA-Z]{2}\s.{1,10}", input))

    def _fetch_address_from_input(self, session: Session, input: str) -> Optional[Address]:
        data = input.split(' ', 1)

        # Uppercase the zip code so that we don't do unneeded API lookups
        zip_code = data[0].upper()
        house_number = data[1]

        address = session\
            .query(Address)\
            .filter_by(zip_code=zip_code, house_number=house_number)\
            .first()

        if address is None:
            address = self._find_address(session, zip_code, house_number)

        return address

    def _find_address(self, session: Session, zip_code: str, house_number: str) -> Address:
        result = self._postcode_api.getaddress(zip_code, house_number)

        # Get or create, just to be safe with regards to concurrency issues
        return get_or_create(
            session, Address, street=result.street, house_number=result.house_number,
            zip_code=result.postcode, city=result.town, country='NL',
            municipality=result.municipality, province=result.province,
            longitude=result.longitude, latitude=result.latitude
        )

    def _get_action_message(self, order: Order) -> str:
        return """Super, ik heb {0} fotos van je ontvangen.

Zou je je postcode en huisnummer willen invullen? Dan zorgen wij dat de fotos goed terecht komen 😉.
b.v.: 1234AB 14a"""\
    .format(len(order.photos))