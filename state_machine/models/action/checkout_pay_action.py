import logging
from typing import Optional

from requests import Response
from sqlalchemy.orm import Session

from core.model.order import Order
from core.model.state import ORDER_CHECKOUT_PAY, START
from core.model.user import User
from core.printapiclient import PrintAPIClient
from state_machine.models.action.action import Action
from state_machine.models.message.message import Message

logger = logging.getLogger(__name__)


class CheckoutPayAction(Action):
    def __init__(self, print_api_client: PrintAPIClient):
        super().__init__()

        self._print_api_client = print_api_client

    def can_handle(self, user: User, message: Message) -> bool:
        return user.state.state == ORDER_CHECKOUT_PAY

    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        order = user.get_order_in_progress(session)
        order_response = self._place_order(order)
        payment_creation_response = self._create_payment(order_response, order)

        order.print_api_payment_url = payment_creation_response.json()['paymentUrl']
        order.print_api_upload_url = order_response.json()['items'][0]['files']['content']['uploadUrl']
        order.in_progress = False
        user.set_state(session, START)
        session.commit()

        self._messages.append(self._get_action_message(order, order.print_api_payment_url))

        return None

    def _place_order(self, order: Order) -> Response:
        order_response = self._print_api_client.place_order(order)

        logger.info(
            'Received order({0}) response: \n [{1}]: {2}'
            .format(order.id, order_response.status_code, order_response.json())
        )

        if order_response.status_code >= 400:
            raise RuntimeError('Failed to get the order response result back from the API')

        return order_response

    def _create_payment(self, order_response: Response, order: Order) -> Response:
        payment_creation_response = self._print_api_client.create_payment(
            order_response.json()['checkout']['setupUrl'],
            order
        )

        logger.info(
            'Received payment creation (order: {0}) response: \n [{1}]: {2}'
            .format(order.id, payment_creation_response.status_code, payment_creation_response.json())
        )

        if payment_creation_response.status_code >= 400:
            raise RuntimeError('Failed to get the payment creation response result back from the API')

        return payment_creation_response

    def _get_action_message(self, order: Order, payment_url: str) -> str:
        return """Aangenaam {0} 😎!

De bestelling kan via de volgende link betaald worden:
{1}

Zodra jij de bestelling betaald hebt, gaan wij aan de slag en zullen we zo spoedig mogelijk de fotos bij je bezorgen!

Alvast bedankt!"""\
            .format(order.recipient_name, payment_url)
