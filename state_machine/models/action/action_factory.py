from typing import List

from pyPostcode import Api

from core.printapiclient import PrintAPIClient
from state_machine.models.action.action import Action
from state_machine.models.action.checkout_address_action import CheckoutAddressAction
from state_machine.models.action.checkout_name_action import CheckoutNameAction
from state_machine.models.action.checkout_pay_action import CheckoutPayAction
from state_machine.models.action.start_action import StartAction
from state_machine.models.action.stop_action import StopAction


class ActionFactory:
    def __init__(self, postcode_api: Api, print_api_client: PrintAPIClient):
        self._postcode_api = postcode_api
        self._print_api_client = print_api_client

    def get_actions(self) -> List[Action]:
        pay_action = CheckoutPayAction(self._print_api_client)
        name_action = CheckoutNameAction(pay_action)
        address_action = CheckoutAddressAction(self._postcode_api, name_action)
        start_action = StartAction(address_action)
        stop_action = StopAction(start_action)

        return [
            stop_action,
            start_action,
            address_action,
            name_action,
            pay_action,
        ]