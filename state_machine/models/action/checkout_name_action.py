from typing import Optional

from sqlalchemy.orm import Session

from core.model.address import Address
from core.model.state import ORDER_CHECKOUT_NAME, ORDER_CHECKOUT_PAY
from core.model.user import User
from state_machine.models.action.action import Action
from state_machine.models.message.message import Message, TextMessage


class CheckoutNameAction(Action):
    def __init__(self, next_action: Action):
        super().__init__()

        self._next_action = next_action

    def can_handle(self, user: User, message: Message) -> bool:
        return user.state.state == ORDER_CHECKOUT_NAME

    def handle(self, session: Session, user: User, message: Message) -> Optional[Action]:
        order = user.get_order_in_progress(session)

        if isinstance(message, TextMessage) and len(message.get_body()) > 2:
            order.recipient_name = message.get_body().strip()
            user.set_state(session, ORDER_CHECKOUT_PAY)
            session.commit()
            return self._next_action

        self._messages.append(self._get_action_message(order.address))

        return None

    def _get_action_message(self, address: Address) -> str:
        return """Dankjewel! De fotos zullen worden bezorgd op:

{0} {1}
{2} {3}

We zijn er bijna, nog één laatste vraag!

Wat is je naam?"""\
            .format(address.street, address.house_number, address.zip_code, address.city)
