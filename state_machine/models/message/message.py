from abc import ABC, abstractmethod


class Message(ABC):
    pass


class TextMessage(Message):
    def __init__(self, body):
        super().__init__()

        self._body = body

    def get_body(self):
        return self._body


class ImageMessage(Message):
    def __init__(self, image_path):
        super().__init__()

        self._image_path = image_path

    def get_image_path(self):
        return self._image_path


class NullMessage(Message):
    pass