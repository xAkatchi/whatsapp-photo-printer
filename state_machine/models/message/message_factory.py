from state_machine.models.message.message import TextMessage, ImageMessage
from whatsapp_bot.media_downloader import MediaDownloader


class MessageFactory:
    def __init__(self, whatsapp_media_downloader: MediaDownloader):
        self._whatsapp_media_downloader = whatsapp_media_downloader

    def create_from_whatsapp_message(self, message):
        if message.getType() == 'text':
            return TextMessage(message.getBody())
        elif message.getType() == 'media' and message.media_type == 'image':
            image_path = self._whatsapp_media_downloader.download(message)

            if image_path:
                return ImageMessage(image_path)

        return None