from typing import Optional

from sqlalchemy.orm import Session

from core.model.user import User
from state_machine.models.action.action import Action
from state_machine.models.action.action_factory import ActionFactory
from state_machine.models.message.message import Message, TextMessage, NullMessage


class StateMachine:
    def __init__(self, action_factory: ActionFactory):
        self._action_factory = action_factory

    def process_message(self, session: Session, user: User, message: Message):
        messages = []
        current_action = self.__get_current_action(user, message)

        while current_action is not None:
            next_action = current_action.handle(session, user, message)
            messages += current_action.get_messages()

            # We set the message to a NullMessage so that the next action is unable
            # to do anything meaningful with the initial message (this will for example
            # prevent cases where an address message is being picked up as the name since
            # it passes the name validation as well)
            message = NullMessage
            current_action = next_action

        return messages

    def __get_current_action(self, user: User, message: Message) -> Optional[Action]:
        for action in self._action_factory.get_actions():
            if action.can_handle(user, message):
                return action

        return None
