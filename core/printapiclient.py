import os
import logging
import sys

import requests
from requests import Response

from core.model.address import Address
from core.model.order import Order

logger = logging.getLogger(__name__)


class PrintAPIClient:
    def __init__(self):
        self.client_id = os.environ.get('PRINT_API_CLIENT_ID', None)
        self.secret = os.environ.get('PRINT_API_SECRET', None)
        self.base_url = os.environ.get('PRINT_API_BASE_URL', None)
        self.return_url = os.environ.get('PRINT_API_RETURN_URL', None)
        self.token = None

        if self.client_id is None or self.secret is None or self.base_url is None or self.return_url is None:
            logger.error('Please add all the PRINT_API environmental variables')
            sys.exit(1)

    def _get_token(self):
        if self.token:
            return self.token

        r = requests.post(
            "{0}/v2/oauth".format(self.base_url),
            data={
                "grant_type": "client_credentials",
                "client_id": self.client_id,
                "client_secret": self.secret
            }
        )

        self.token = r.json()["access_token"]

        return self.token

    def get_order_data(self, order_id: str) -> Response:
        return requests.get(
            "{0}/v2/orders/{1}".format(self.base_url, order_id),
            headers={
                "Authorization": "Bearer " + self._get_token()
            }
        )

    def create_payment(self, payment_url: str, order: Order) -> Response:
        payload = {
            "returnUrl": self.return_url,
            "billing": {
                "address": {
                    "name": order.recipient_name,
                    "line1": "{0} {1}".format(order.address.street, order.address.house_number),
                    "postCode": order.address.zip_code,
                    "city": order.address.city,
                    "country": order.address.country
                }
            }
        }

        return requests.post(
            payment_url,
            json=payload,
            headers={
                "Authorization": "Bearer " + self._get_token()
            }
        )

    def place_order(self, order: Order) -> Response:
        payload = {
            "id": order.external_id,
            "email": "whatsapp-foto-printer@codekrijger.io",
            "items": [{
                "productId": order.product.printapi_id,
                "pageCount": len(order.photos),
                "quantity": 1,
                "options": [
                    {"id": "finish", "value": "uv_gloss"}
                ]
            }],
            "shipping": {
                "preference": "auto",
                "address": {
                    "name": order.recipient_name,
                    "line1": "{0} {1}".format(order.address.street, order.address.house_number),
                    "postCode": order.address.zip_code,
                    "city": order.address.city,
                    "country": order.address.country
                }
            }
        }

        r = requests.post(
            "{0}/v2/orders".format(self.base_url),
            json=payload,
            headers={
                "Authorization": "Bearer " + self._get_token()
            }
        )

        return r

    def upload_pdf(self, url, pdf_path) -> Response:
        return requests.post(
            url,
            data=open(pdf_path, 'rb'),
            headers={
                'Authorization': 'Bearer ' + self._get_token(),
                'Content-Type': 'application/pdf'
            }
        )