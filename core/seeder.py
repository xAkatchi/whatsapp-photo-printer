from core.model import product, state
from core.model.product import Product
from core.model.state import State
from core.model.utils import get_or_create


def seed_database(session):
    get_or_create(session, State, state=state.START)
    get_or_create(session, State, state=state.ORDER_CHECKOUT_ADDRESS)
    get_or_create(session, State, state=state.ORDER_CHECKOUT_NAME)
    get_or_create(session, State, state=state.ORDER_CHECKOUT_PAY)

    get_or_create(
        session, Product, description=product.PHOTO_15x10, printapi_id="fotoprints_15x10",
        price_per_piece=39, pdf_width=156, pdf_height=106
    )
