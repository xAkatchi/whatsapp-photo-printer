import uuid
from datetime import datetime

from sqlalchemy import Integer, Column, ForeignKey, DateTime, Boolean, String
from sqlalchemy.orm import relationship

from core.model.base import Base


class Order(Base):
    __tablename__ = "order"

    id = Column(Integer, primary_key=True)
    external_id = Column(String(255), unique=True, nullable=False)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    user = relationship("User", back_populates="orders")
    product_id = Column(Integer, ForeignKey("product.id"), nullable=True)
    product = relationship("Product", back_populates="orders")
    address_id = Column(Integer, ForeignKey("address.id"), nullable=True)
    address = relationship("Address", back_populates="orders")
    recipient_name = Column(String(50), nullable=True)
    photos = relationship("Photo", back_populates="order")
    in_progress = Column(Boolean, default=True)
    pdf_file_path = Column(String(255), unique=True, nullable=True)
    print_api_upload_url = Column(String(255), unique=True, nullable=True)
    print_api_payment_url = Column(String(255), unique=True, nullable=True)
    created_at = Column(DateTime, default=datetime.now(), nullable=False)

    def __init__(self, user):
        self.user = user
        self.external_id = uuid.uuid4()