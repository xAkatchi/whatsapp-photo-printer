from sqlalchemy import Integer, Column, String, ForeignKey
from sqlalchemy.orm import relationship

from core.model.base import Base
from core.model.order import Order
from core.model.state import State
from core.model.utils import get_or_create


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    phone_number = Column(String(50), unique=True, nullable=False)
    messages = relationship("Message", back_populates="sender")
    state_id = Column(Integer, ForeignKey("state.id"), nullable=False, default=1)
    orders = relationship("Order", back_populates="user")
    state = relationship("State", back_populates="users")

    def __init__(self, phone_number):
        self.phone_number = phone_number

    def set_state(self, session, state):
        self.state = get_or_create(session, State, state=state)

    def get_order_in_progress(self, session):
        return session \
            .query(Order) \
            .filter_by(user=self, in_progress=True) \
            .order_by(Order.id.desc()) \
            .first()