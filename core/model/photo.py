from datetime import datetime

from sqlalchemy import Integer, Column, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from core.model.base import Base


class Photo(Base):
    __tablename__ = "photo"

    id = Column(Integer, primary_key=True)
    file_path = Column(String(255), unique=True, nullable=False)
    order_id = Column(Integer, ForeignKey("order.id"), nullable=False)
    order = relationship("Order", back_populates="photos")
    uploaded_at = Column(DateTime, default=datetime.now(), nullable=False)

    def __init__(self, file_path, order):
        self.file_path = file_path
        self.order = order
