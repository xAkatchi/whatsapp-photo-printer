from sqlalchemy import Integer, Column, String
from sqlalchemy.orm import relationship

from core.model.base import Base

START = "START"
ORDER_CHECKOUT_ADDRESS = "ORDER:CHECKOUT:ADDRESS"
ORDER_CHECKOUT_NAME = "ORDER:CHECKOUT:NAME"
ORDER_CHECKOUT_PAY = "ORDER:CHECKOUT:PAY"


class State(Base):
    __tablename__ = "state"

    id = Column(Integer, primary_key=True)
    state = Column(String(50), unique=True, nullable=False)
    users = relationship("User", back_populates="state")
    messages = relationship("Message", back_populates="user_state")

    def __init__(self, state):
        self.state = state
