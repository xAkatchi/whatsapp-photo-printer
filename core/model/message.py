from datetime import datetime

from sqlalchemy import Integer, Column, String, ForeignKey, Text, DateTime
from sqlalchemy.orm import relationship

from core.model.base import Base


class Message(Base):
    __tablename__ = "message"

    id = Column(Integer, primary_key=True)
    external_id = Column(String(50), unique=True, nullable=False)
    sender_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    sender = relationship("User", back_populates="messages")
    message = Column(Text, nullable=False)
    user_state_id = Column(Integer, ForeignKey("state.id"), nullable=False, default=1)
    user_state = relationship("State", back_populates="messages")
    timestamp = Column(DateTime, nullable=False)
    type = Column(String(50))

    def __init__(self, external_id, sender, message, timestamp, type, state):
        self.external_id = external_id
        self.sender = sender
        self.message = message
        self.timestamp = datetime.fromtimestamp(timestamp)
        self.type = type
        self.user_state = state
