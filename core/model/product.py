from sqlalchemy import Integer, Column, String
from sqlalchemy.orm import relationship

from core.model.base import Base

PHOTO_15x10 = "Foto | 15cm x 10cm"


class Product(Base):
    __tablename__ = "product"

    id = Column(Integer, primary_key=True)
    description = Column(String(50), unique=True, nullable=False)
    printapi_id = Column(String(50), unique=True, nullable=False)
    price_per_piece = Column(Integer, comment="In euro cents")
    pdf_width = Column(Integer, comment="In mm")
    pdf_height = Column(Integer, comment="In mm")
    orders = relationship("Order", back_populates="product")

    def __init__(self, description, printapi_id, price_per_piece, pdf_width, pdf_height):
        self.description = description
        self.printapi_id = printapi_id
        self.price_per_piece = price_per_piece
        self.pdf_width = pdf_width
        self.pdf_height = pdf_height
