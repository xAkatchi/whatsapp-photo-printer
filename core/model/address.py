from sqlalchemy import Integer, Column, String
from sqlalchemy.orm import relationship

from core.model.base import Base


class Address(Base):
    __tablename__ = "address"

    id = Column(Integer, primary_key=True)
    street = Column(String(150), nullable=False)
    house_number = Column(String(10), nullable=False)
    zip_code = Column(String(10), nullable=False)
    city = Column(String(100), nullable=False)
    country = Column(String(100), nullable=False, comment="ISO 3166-1 alpha 2")
    municipality = Column(String(100), nullable=False)
    province = Column(String(100), nullable=False)
    longitude = Column(String(20), nullable=False)
    latitude = Column(String(20), nullable=False)
    orders = relationship("Order", back_populates="address")

    def __init__(self, street, house_number, zip_code, city, country, municipality, province, longitude, latitude):
        self.street = street
        self.house_number = house_number
        self.zip_code = zip_code
        self.city = city
        self.country = country
        self.municipality = municipality
        self.province = province
        self.longitude = longitude
        self.latitude = latitude
