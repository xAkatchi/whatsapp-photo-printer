import logging
import os
import pathlib
import sentry_sdk
import flask
from datetime import datetime

from flask import Flask, send_from_directory
from sentry_sdk.integrations.flask import FlaskIntegration

from core.model.base import session_factory
from core.model.order import Order
from core.model.user import User
from core.model.product import Product
from core.model.photo import Photo
from core.model.message import Message
from core.printapiclient import PrintAPIClient
from api.utils.image_to_pdf_converter import ImageToPDFConverter


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
sentry_sdk.init(
    dsn=os.environ.get('SENTRY_DSN'),
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)
logger = logging.getLogger(__name__)


@app.route('/')
def home():
    return app.send_static_file('index.html')


@app.route('/thank-you')
def thank_you():
    return app.send_static_file('thank-you.html')


@app.route("/KVC7MKU_XZlNyIM0IOEvIomVlDufwiyCxq9rr47rNeTi!6dkWr!Mncc7B4", methods=['POST'])
def payment_hook():
    try:
        if 'orderId' not in flask.request.form:
            flask.abort(404, 'Unknown order id')

        print_api_client = PrintAPIClient()
        external_order_id = flask.request.form['orderId']
        order_response = print_api_client.get_order_data(external_order_id)

        logger.info(
            'Received order response (order: {0}): \n [{1}]: {2}'
            .format(external_order_id, order_response.status_code, order_response.json())
        )

        if order_response.status_code >= 400:
            logger.error(
                'Failed to get the order data (order: {0}): \n [{1}]: {2}'
                .format(external_order_id, order_response.status_code, order_response.json())
            )
            flask.abort(400, 'Error')

        if order_response.json()['checkout']['status'] != 'Successful':
            logger.error(
                'The order ({0}) hasn\'t been paid yet'
                .format(external_order_id)
            )
            flask.abort(400, 'Error')

        # Update the order in the database.
        session = session_factory()
        order = session.query(Order).filter_by(external_id=external_order_id).first()

        if order is None:
            flask.abort(400, 'Error')

        pdf_path = _create_pdf_upload_path(order)

        converter = ImageToPDFConverter(order.product.pdf_width, order.product.pdf_height)
        converter.create_pdf(_fill_photos(order.photos, 8), pdf_path)
        order.pdf_file_path = pdf_path

        upload_response = print_api_client.upload_pdf(order.print_api_upload_url, pdf_path)

        session.commit()

        logger.info(
            'Received upload response (order: {0}): \n [{1}]: {2}'
            .format(order.id, upload_response.status_code, upload_response.json())
        )

        if upload_response.status_code >= 400:
            logger.error(
                'Failed to upload the PDF (order: {0}): \n [{1}]: {2}'
                .format(order.id, upload_response.status_code, upload_response.json())
            )
            flask.abort(400, 'Error')

        return 'Ok'
    except Exception as exception:
        flask.abort(500, 'API call failed: {error}'.format(error=exception))


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory('static/fonts', path)


@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('static/img', path)


def _fill_photos(photos, minimum_length):
    if len(photos) > minimum_length:
        return photos

    while len(photos) < minimum_length:
        photos += photos.copy()

    return photos[:minimum_length]


def _create_pdf_upload_path(order):
    now = datetime.now()

    folder_path = "/mnt/nfs/storage/pdfs/{0}/{1}/{2}/" \
        .format(now.year, now.month, now.day)

    path = "{0}order-{1}.pdf" \
        .format(folder_path, order.id)

    pathlib.Path(folder_path).mkdir(parents=True, exist_ok=True)

    count = 0

    while os.path.exists(path):
        count += 1
        path = "{0}order-{1}_{2}.pdf" \
            .format(folder_path, order.id, count)

    return path