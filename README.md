# Register

config-cc: https://www.ipipi.com/networkList.do
config-mcc: https://en.wikipedia.org/wiki/Mobile_country_code#N
config-mnc: https://en.wikipedia.org/wiki/Mobile_country_code#N

### Step 1

```bash
yowsup-cli registration \
    --requestcode sms \
    --config-phone="31617917186" \
    --config-cc="31" \
    --config-pushname="Print je foto" \
    --config-mcc=204 \
    --config-mnc=10 
```

### Step 2

```bash
yowsup-cli registration --register "226899" --config-phone "31617917186"
---

yowsup-cli  v3.2.0
yowsup      v3.2.3

Copyright (c) 2012-2019 Tarek Galal
http://www.openwhatsapp.org

This software is provided free of charge. Copying and redistribution is
encouraged.

If you appreciate this software and you would like to support future
development please consider donating:
http://openwhatsapp.org/yowsup/donate


I 2019-05-18 15:04:20,598 yowsup.common.http.warequest - b'{"status":"ok","login":"31683837119","type":"new","edge_routing_info":"CAUIAg==","chat_dns_domain":"fb","security_code_set":false}\n'
{
    "__version__": 1,
    "cc": "31",
    "client_static_keypair": "ABZxezjLl9JbfIBff1VgfejehADmc7VIg4zVL3/OlEaDce+pSQ3+Icp6q8Ryw7eQqp5RsNnRjBhNVxdaPEUoDg==",
    "expid": "L18L/FJ2SkGGVaNQWo9ckw==",
    "fdid": "9e636a34-3137-4641-b05f-431795ad2654",
    "id": "dT2dO1S7A74z0KNuY/6Ui6dLJSw=",
    "mcc": "204",
    "mnc": "10",
    "phone": "31683837119",
    "pushname": "Print je foto",
    "sim_mcc": "000",
    "sim_mnc": "000"
}
status: b'ok'
login: b'31683837119'
type: b'new'
edge_routing_info: b'CAUIAg=='
```