### V1.0
- [x] Replace the Mollie client with the printapi
- [x] Use fake order ids to not expose our internal numbering
- [x] Cleanup the checkout_pay_action to be easier to read
- [x] Cleanup unused classes (mollie client, payment model stuff like that)
- [x] Finalize production environment credentials (also different sentry and such?)
- [x] Update printapi so we can use their payment thing
- [x] Add error handling to the network calls (throwing exceptions?)

### V1.1
- [ ] bin/scripts/whatsapp_register_number.sh Make path independant
- [ ] Add the yowsup folder to the gitignore
- [ ] Replace the exception throwing on errors with a graceful message to try again
- [ ] Fix errors inside the webhook callback
- [ ] Add support for the unhappy flow when users cancel their payment
- [ ] Instead of saving messages with: `whatsapp_bot.layer.EchoLayer#__saveMessage` refactor this to a more central location that can use the central messages from the system
- [ ] Make the configuration variables for the PrintAPIClient injectable? Or find another good way to pass those
- [ ] Rename the bin/docker/api to web (including the flask api folder to web and such as well)
- [ ] Centralize the address to array format inside the `printapiclient.py`

### V2.0
- [ ] Add Telegram support
- [ ] Split up the requiprements per project
- [ ] Capitalize all variables inside the scripts

### Uncategorized
- [ ] Add backup support for the database
